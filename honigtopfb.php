<?php
	session_set_cookie_params (2592000, "/", "springer.pk", True, True);
	session_name("itcshonigtopf");
	session_start();
	date_default_timezone_set('Asia/Singapore');
	$zeit = date("Y-m-d H:i:s");
	$id = '';
	$anzahl = 0;
	$reversedns = gethostbyaddr($_SERVER['REMOTE_ADDR']);
	$whois = trim(str_replace('netname:', '', shell_exec('whois ' . $_SERVER['REMOTE_ADDR'] . ' | grep netname')));
	
	if ( isset($_SESSION["id"]) ){
		$id = $_SESSION["id"];
		$anzahl = $_SESSION["anzahl"] + 1;
		$_SESSION["anzahl"] = $anzahl;
	}
	else {
		$id = hash('sha256', random_bytes(8) . $zeit);
		$_SESSION["id"] = $id;
		$_SESSION["anzahl"] = 1;
	}
?>
<!DOCTYPE html>
<html lang="de">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Honigtopf für Unterrichtszwecke</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	</head>

	<body class="bg-dark text-light">
		<h1>(öffentliche) Informationen</h1>
		<p>
			<a href="honigtopfb.php">neuladen</a>
		</p>
		<div class="table-responsive">
			<table class="table table-sm table-dark" style="font-size: 0.7em;">
				<thead>
					<tr>
						<th scope="col">Eigenschaft</th>
						<th scope="col">Wert</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>IP</td>
						<td><?php echo $_SERVER['REMOTE_ADDR'];?></td>
					</tr>
					<tr>
						<td>GeoIP</td>
						<td>...</td>
					</tr>
					<tr>
						<td>MLS</td>
						<td>...</td>
					</tr>
					<tr>
						<td>Port</td>
						<td><?php echo $_SERVER['REMOTE_PORT'];?></td>
					</tr>
					<tr>
						<td>Browser und OS</td>
						<td><?php echo $_SERVER['HTTP_USER_AGENT'];?></td>
					</tr>
					<tr>
						<td>Cookie-ID</td>
						<td><?php echo $id;?></td>
					</tr>
					<tr>
						<td>Anzahl der Besuche</td>
						<td><?php echo $anzahl;?></td>
					</tr>
					<tr>
						<td>Zeitstempel</td>
						<td><?php echo $zeit;?></td>
					</tr>
					<tr>
						<td>HTTP_ACCEPT_LANGUAGE</td>
						<td><?php echo $_SERVER['HTTP_ACCEPT_LANGUAGE'];?></td>
					</tr>
					<tr>
						<td>HTTP_ACCEPT_ENCODING</td>
						<td><?php echo $_SERVER['HTTP_ACCEPT_ENCODING'];?></td>
					</tr>
					<tr>
						<td>HTTP_REFERER</td>
						<td><?php echo $_SERVER['HTTP_REFERER'];?></td>
					</tr>
					<tr>
						<td>reverse DNS</td>
						<td><?php echo $reversedns;?></td>
					</tr>
					<tr>
						<td>whois</td>
						<td><?php echo $whois;?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</body>
</html>